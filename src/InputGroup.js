import React, { Component } from "react";
import PropTypes from "prop-types";
import Row from "./Row";

class InputGroup extends Component {
  constructor(props) {
    super()
    console.log('ctr', props)
    this.state = {    
      values: props.fields.map(x => x.defaultValue || '')
    };
  }

  value() {
    return this.state.values.reduce((acc, x) => {
      acc += x
      return acc
    },'')
  }

  handleChange = event => {    
    const ndx = event.target.name.split('').pop() -1

    const newValues = this.state.values.map((x,i) => {
      return ndx === i ? event.target.value : x
    })
    
    this.setState({
      values: newValues
    })
  }
  render() {    
    return (
      <Row>
        {this.props.fields.map((f, i) => (
          <div key={i}>
            <input  
              name={`InputGroupField${i + 1}`} 
              value={ this.state.values[i] }
              onChange={ e => this.handleChange(e) }
            />
            {" "}
            {i < this.props.fields.length - 1 && `${this.props.seperator}`}
            {" "}
          </div>
        ))}
      </Row>
    );
  }
}

InputGroup.propTypes = {
  seperator: PropTypes.string,
  value: PropTypes.string,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      validator: PropTypes.string,
      defaultValue: PropTypes.string,
      maxLength: PropTypes.number,
      width: PropTypes.number
    })
  )
};

InputGroup.defaultProps = {
  seperator: ""
};

export default InputGroup;
