import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const RowWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
`

const Row = ({ children, ...props }) => {
  const styles = () => (
    {
      padding: `${props.gutter}`
    }
  )
  return (
    <RowWrapper style={styles()} {...props}>
      {children}
    </RowWrapper>
  )
}

Row.propTypes = {
  children: PropTypes.node,
  gutter: PropTypes.string
}

Row.defaultProps = {
  gutter: '0'
}

export default Row