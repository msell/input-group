import React from 'react';
import { render } from 'react-dom';
import SSNInput from './SSNInput';

const styles = {
  fontFamily: 'sans-serif',
  textAlign: 'center',
};

const App = () => (
  <div style={styles}>    
    <SSNInput />
  </div>
);

render(<App />, document.getElementById('root'));
